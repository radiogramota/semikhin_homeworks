package attestation01_oop;

import java.util.List;


public class Main {
    public static void main(String[] args) {
        UsersRepository usersRepository = new UsersRepositoryFileImpl("путь к файлу\\users.txt");
        User usersById = usersRepository.findById(1);
        System.out.println(usersById.getId() + " " + usersById.getAge() + " " + usersById.getName() + " " + usersById.isWorker());
        // создалим нового User с id=2 и обновим файл
        User userMars = new User(2, "Mars", 32, true);
        usersRepository.save(List.of(usersById, userMars));
        // сделаем поиск по id = 2, выведем инф-ию об обьекте
        User userId2 = usersRepository.findById(2);
        System.out.println(userId2.getName() + " " + userId2.getAge());
        // изменим User с id2, обновим файл, сделаем вывод информации
        userId2.setName("Bars");
        userId2.setAge(42);
        usersRepository.update(userId2);
        System.out.println(userId2.getName() + " " + userId2.getAge());
    }
}

/* результат выполнения
1 33 Игорь true
Mars 32
Bars 42*/

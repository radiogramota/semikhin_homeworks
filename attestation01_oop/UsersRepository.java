package attestation01_oop;

import java.util.List;

public interface UsersRepository {
    User findById(int Id);

    void save(List<User> users);

    void update(User user);
}

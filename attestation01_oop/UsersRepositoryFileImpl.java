package attestation01_oop;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * 15.11.2021
 * 20. Java IO
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class UsersRepositoryFileImpl implements UsersRepository {

    private final String fileName;

    public UsersRepositoryFileImpl(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public void save(List<User> users) {
        Writer writer = null;
        BufferedWriter bufferedWriter = null;
        try {
            writer = new FileWriter(fileName, false);
            bufferedWriter = new BufferedWriter(writer);
            for (int i = 0; i < users.size(); i++) {
                User user = users.get(i);
                bufferedWriter.write(user.getId() + "|" + user.getName() + "|" + user.getAge() + "|" + user.isWorker());
                bufferedWriter.newLine();
                bufferedWriter.flush();
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException ignore) {
                }
            }
            if (bufferedWriter != null) {
                try {
                    bufferedWriter.close();
                } catch (IOException ignore) {
                }
            }
        }
    }


    @Override
    public User findById(int id) {
        List<User> users = new ArrayList<>();
        Reader reader = null;
        BufferedReader bufferedReader = null;
        try {
            reader = new FileReader(fileName);
            bufferedReader = new BufferedReader(reader);
            String line = bufferedReader.readLine();
            while (line != null) {
                String[] parts = line.split("\\|");
                int currentId = Integer.parseInt(parts[0]);
                if (currentId == id) {
                    String name = parts[1];
                    int age = Integer.parseInt(parts[2]);
                    boolean isWorker = Boolean.parseBoolean(parts[3]);
                    User newUser = new User(id, name, age, isWorker);
                    users.add(newUser);
                }
                line = bufferedReader.readLine();
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException ignore) {
                }
            }
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (IOException ignore) {
                }
            }
        }
        if (users.size() > 0) {
            return users.get(0);
        } else {
            return new User(0, "", 0, false);
        }
    }

    @Override
    public void update(User user) {
        List<User> users = new ArrayList<>();
        try (Reader reader = new FileReader(fileName); BufferedReader bufferedReader = new BufferedReader(reader)) {
            String line = bufferedReader.readLine();
            while (line != null) {
                String[] parts = line.split("\\|");
                int id = Integer.parseInt(parts[0]);
                if (id == user.getId()) {
                    users.add(user);
                } else {
                    String name = parts[1];
                    int age = Integer.parseInt(parts[2]);
                    boolean isWorker = Boolean.parseBoolean(parts[3]);
                    User newUser = new User(id, name, age, isWorker);
                    users.add(newUser);
                }
                line = bufferedReader.readLine();
            }

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        save(users);
    }
}


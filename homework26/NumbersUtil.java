package ru.ms.homework26;

public class NumbersUtil {
    // простое число, это число, которое делится на само себя и на единицу и все :)
    // 31 -> 2, 3, 4, 5, 6, 7, ...., 30
    // 31 -> 2, 3, 4, ..., 15
    // 31 -> 2, 3, 4, 5
    public boolean isPrime(int number) {

        if (number == 0 || number == 1) {
            throw new IllegalArgumentException();
        }

        if (number == 2 || number == 3) {
            return true;
        }

        for (int i = 2; i * i <= number; i++) {
            if (number % i == 0) {
                return false;
            }
        }

        return true;
    }

    public int sum(int a, int b) {
        return a + b;
    }

    /*
    нод(18, 12) -> 6
    нод(9, 12) -> 3
    нод(64, 48) -> 16
    Предусмотреть, когда на вход "некрсивые числа", отрицательные числа -> исключения
     */
    public int gcd(int a, int b) {
        if (a < 0 || b < 0) {throw new IllegalArgumentException();}
        if (a%b == 0) {return b;}
        if (b%a == 0) {return a;}
        if (a > b) {return gcd(a%b, b);}
        else {return gcd(a, b%a);}
        }
    }
